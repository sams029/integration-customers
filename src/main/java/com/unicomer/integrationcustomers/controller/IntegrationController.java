package com.unicomer.integrationcustomers.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.unicomer.integrationcustomers.DTO.CustomerReq;
import com.unicomer.integrationcustomers.DTO.CustomerRsp;
import com.unicomer.integrationcustomers.service.RedirectService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/integration/customers")
public class IntegrationController {

	@Autowired
	RedirectService redirectService;

	@GetMapping("")
	public ResponseEntity<?> getCustomers(@RequestHeader("country") String countryCode) {

		try {

			log.info("Checking country...");
			if (!redirectService.checkingCountry(countryCode)) {
				return new ResponseEntity<>(
						new CustomerRsp("Invalid", "Country code is not allowed", Long.parseLong("0")), HttpStatus.BAD_REQUEST);
			}

			log.info("Getting customers...");
			return new ResponseEntity<>(redirectService.redirectGetCustomers(countryCode), HttpStatus.OK);

		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/identification/{id}")
	public ResponseEntity<?> getCustomerByIdentification(@RequestHeader("country") String countryCode,
			@PathVariable String id) {

		try {

			log.info("Checking country...");
			if (!redirectService.checkingCountry(countryCode)) {
				return new ResponseEntity<>(
						new CustomerRsp("Invalid", "Country code is not allowed", Long.parseLong("0")), HttpStatus.BAD_REQUEST);
			}

			log.info("Getting customers by Identification...");
			return new ResponseEntity<>(redirectService.redirectGetCustomerByIdentification(countryCode, id),
					HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("")
	public ResponseEntity<CustomerRsp> createCustomer(@Valid @RequestBody CustomerReq customerReq, Errors errors,
			@RequestHeader("country") String countryCode) {

		try {

			log.info("Checking data... ");
			if (errors.hasErrors()) {
				return new ResponseEntity<>(
						new CustomerRsp("Validate", errors.getFieldError().getDefaultMessage(), Long.parseLong("0")),
						HttpStatus.BAD_REQUEST);
			}

			log.info("Checking country...");
			if (!redirectService.checkingCountry(countryCode)) {
				return new ResponseEntity<>(
						new CustomerRsp("Invalid", "Country code is not allowed", Long.parseLong("0")), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<>(redirectService.redirectSaveCustomer(countryCode, customerReq), HttpStatus.OK);

		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(
					new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<CustomerRsp> changeCustomer(@Valid @RequestBody CustomerReq customerReq, Errors errors,
			@PathVariable Long id, @RequestHeader("country") String countryCode) {

		try {

			log.info("Checking data... ");
			if (errors.hasErrors()) {
				return new ResponseEntity<>(
						new CustomerRsp("Validate", errors.getFieldError().getDefaultMessage(), Long.parseLong("0")),
						HttpStatus.BAD_REQUEST);
			}

			log.info("Checking country...");
			if (!redirectService.checkingCountry(countryCode)) {
				return new ResponseEntity<>(
						new CustomerRsp("Invalid", "Country code is not allowed", Long.parseLong("0")), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<>(redirectService.redirectChangeCustomer(countryCode, customerReq, id), HttpStatus.OK);

		} catch (Exception e) {
			log.error(e.toString());
			return new ResponseEntity<>(
					new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
