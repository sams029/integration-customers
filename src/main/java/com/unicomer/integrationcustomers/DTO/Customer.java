package com.unicomer.integrationcustomers.DTO;

import javax.validation.Valid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Valid
@Builder(toBuilder = true)
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

	private Long idCustomer;

	private String customerIdentification;

	private String firstName;

	private String lastName;

	private String birthday;

	private Character gender;

	private String cellPhone;

	private String homePhone;

	private String addressHome;

	private String profession;

	private BigDecimal incomes;
    
}
