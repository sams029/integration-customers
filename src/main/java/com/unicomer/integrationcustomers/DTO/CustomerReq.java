package com.unicomer.integrationcustomers.DTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Valid
@Builder(toBuilder = true)
public class CustomerReq {


    @NotNull(message = "Customer identification must not be null")
	@Size(min = 1, max = 20, message = "Customer identification must be between 1 to 20 characters")
	private String customerIdentification;

	@NotNull(message = "First name must not be null")
	@Size(min = 1, max = 75, message = "First name must be between 1 to 75 characters")
	private String firstName;

	@NotNull(message = "Last name must not be null")
	@Size(min = 1, max = 75, message = "Last name must be between 1 to 75 characters")
	private String lastName;

	@NotNull(message = "Birthday must not be null")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime birthday;

	@NotNull(message = "Gender must not be null")
	private Character gender;

	@NotNull(message = "Cell Phone must not be null")
	@Size(min = 7, max = 15, message = "Cell Phone must be between 7 to 15 characters")
	private String cellPhone;

	@NotNull(message = "Home phone must not be null")
	@Size(min = 7, max = 15, message = "Home phone must be between 7 to 15 characters")
	private String homePhone;

	@NotNull(message = "Address home must not be null")
	@Size(min = 1, max = 300, message = "Address home must be between 1 to 300 characters")
	private String addressHome;

	@NotNull(message = "Profession must not be null")
	@Size(min = 0, max = 70, message = "Profession can not be more than 70 character")
	private String profession;

	@NotNull(message = "Incomes must not be null")
	private BigDecimal incomes;
    
}

