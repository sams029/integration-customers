package com.unicomer.integrationcustomers.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CustomerRsp {

	private String status;
	private String description;
	private Long idCustomer;
	
}
