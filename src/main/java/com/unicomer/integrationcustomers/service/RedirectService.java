package com.unicomer.integrationcustomers.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.unicomer.integrationcustomers.DTO.Customer;
import com.unicomer.integrationcustomers.DTO.CustomerReq;
import com.unicomer.integrationcustomers.DTO.CustomerRsp;
import com.unicomer.integrationcustomers.configuration.CountryCodes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RedirectService {

    @Autowired
    RestTemplate restTemplate;

    @Value("${jamaica.url}")
    private String urlJamaica;
    @Value("${guatemala.url}")
    private String urlGuatemala;
    @Value("${costarica.url}")
    private String urlCostaRica;

    @Value("${jamaica.customer.path}")
    private String pathJamaica;
    @Value("${guatemala.customer.path}")
    private String pathGuatemala;
    @Value("${costarica.customer.path}")
    private String pathCostaRica;

    int exist = 0;
    String baseUrlCountry = "";
    Gson g = new Gson();

    public List<Customer> redirectGetCustomers(String countryCode) {

        try {

            getUrlCountry(countryCode);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<CustomerReq> entity = new HttpEntity<CustomerReq>(headers);

            return g.fromJson(restTemplate.exchange(baseUrlCountry, HttpMethod.GET, entity,
                    String.class).getBody(), new TypeToken<List<Customer>>() {
                    }.getType());

        } catch (Exception e) {
            log.error(e.toString());
            return new ArrayList<Customer>();
        }

    }

    public List<Customer> redirectGetCustomerByIdentification(String countryCode, String identification) {

        try {

            getUrlCountry(countryCode);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<CustomerReq> entity = new HttpEntity<CustomerReq>(headers);

            return g.fromJson(
                    restTemplate.exchange(baseUrlCountry + "/identification/" + identification, HttpMethod.GET, entity,
                            String.class).getBody(),
                    new TypeToken<List<Customer>>() {
                    }.getType());

        } catch (Exception e) {
            log.error(e.toString());
            return new ArrayList<Customer>();
        }

    }

    public CustomerRsp redirectSaveCustomer(String countryCode, CustomerReq customerReq) {

        try {

            getUrlCountry(countryCode);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<CustomerReq> entity = new HttpEntity<CustomerReq>(customerReq, headers);

            return g.fromJson(restTemplate.exchange(baseUrlCountry, HttpMethod.POST, entity,
                    String.class).getBody(), CustomerRsp.class);

        } catch (Exception e) {
            log.error(e.toString());
            return new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0"));
        }

    }

    public CustomerRsp redirectChangeCustomer(String countryCode, CustomerReq customerReq, Long id) {

        try {

            getUrlCountry(countryCode);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<CustomerReq> entity = new HttpEntity<CustomerReq>(customerReq, headers);

            return g.fromJson(restTemplate.exchange(baseUrlCountry + "/" + id, HttpMethod.PUT, entity,
                    String.class).getBody(), CustomerRsp.class);

        } catch (Exception e) {
            log.error(e.toString());
            return new CustomerRsp("Error", "Please contact technical support", Long.parseLong("0"));
        }

    }

    public boolean checkingCountry(String countryCode) {

        for (CountryCodes code : CountryCodes.values()) {
            if (code.name().equals(countryCode)) {
                return true;
            }
        }

        return false;
    }

    public void getUrlCountry(String countryCode) {

        exist = 0;
        baseUrlCountry = "";


        switch (countryCode) {
            case "JAM":
                log.info(urlJamaica+pathJamaica);
                baseUrlCountry = urlJamaica + pathJamaica;
                break;
            case "GTM":
                log.info(urlGuatemala+pathGuatemala);
                baseUrlCountry = urlGuatemala + pathGuatemala;
                break;
            case "CRI":
                log.info(urlCostaRica+pathCostaRica);
                baseUrlCountry = urlCostaRica + pathCostaRica;
                break;
        }
    }

}
